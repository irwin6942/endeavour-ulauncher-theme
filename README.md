EndeavourOS inspired Ulauncher theme. Using the color codes in the i3WM configuration file from EndeavourOS. Transparent greys with purple and blue accents. Fork of https://github.com/lifeofcoding/transparent-blue-accent-ulauncher-theme.

![Screenshot](./screenshot.png?raw=true)

# Installation

Copy or `git clone` this repo to `$HOME/.config/ulauncher/user-themes/`.
